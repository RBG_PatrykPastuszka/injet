﻿using System.Collections.Generic;
using System.Linq;
using ITnnovative.InJetSystem.Attributes;  
using Mono.Cecil;
using Mono.Cecil.Cil;
using Mono.Cecil.Rocks;
using UnityEditor;
using UnityEngine; 
   
namespace ITnnovative.InJetSystem.Editor
{
    [InitializeOnLoad]
    public class InJetAssemblyProcessor
    {
        
        static InJetAssemblyProcessor()
        {
            // Get assembly
            var assembly = AssemblyDefinition.ReadAssembly(Application.dataPath+"/../Library/ScriptAssemblies/Assembly-CSharp.dll"
            , new ReaderParameters(){ ReadWrite = true });

            // Find method for InJet Processing
            var ctorMethod = null as MethodDefinition;
            var awakeMethod = null as MethodDefinition;
            
            
            // Get all modules
            foreach (var module in assembly.Modules)
            {
                // Get all types
                foreach (var type in module.Types)
                {
                    if (type.HasMethods)
                    {

                        foreach (var m in type.Methods)
                        {
                            if (m.Name == "ProcessInJetCtor")
                            {
                                ctorMethod = m;
                            }
                            else if (m.Name == "ProcessInJetAwake")
                            {
                                awakeMethod = m;
                            }
                        }
                    }
                }
            }
            
            // Get all modules
            foreach (var module in assembly.Modules)
            {
                // Get all types
                foreach (var type in module.Types)
                {
                    if (type.HasMethods)
                    {
                        /*foreach (var property in type.Properties)
                        {
                            if (property.HasCustomAttributes) 
                            {
                                // TODO: Maybe modify GET method?
                                if (property.CustomAttributes.Any(q => 
                                    q.AttributeType.FullName == (typeof(InJetAttribute)).FullName))
                                {
                                    // Process type
                                    ProcessType(type, ctorMethod, awakeMethod);
                                    goto next_type;
                                }
                            } 
                        }*/
                        foreach (var field in type.Fields)
                        {
                            if (field.HasCustomAttributes) 
                            {
                                if (field.CustomAttributes.Any(q => 
                                    q.AttributeType.FullName == (typeof(InJetAttribute)).FullName))
                                {
                                    // Process type
                                    ProcessType(type, ctorMethod, awakeMethod);
                                    goto next_type;
                                }
                            } 
                        }
                    }

                    next_type: ;
                }
               
                module.Write();
                //module.Write(Application.dataPath+"/../Library/ScriptAssemblies/Assembly-CSharp.dll");        
            }
            
        }

        /// <summary>
        /// Process the type - register and add InJets
        /// </summary>
        public static void ProcessType(TypeDefinition type,
            MethodDefinition ctorMethod, MethodDefinition awakeMethod)
        {
            var startMethod = type.Methods.FirstOrDefault(m => m.Name == "Awake");
            if (startMethod == null)
            {
                startMethod = type.Methods.FirstOrDefault(m => m.Name == "OnEnable");
                if (startMethod == null)
                {
                    startMethod = type.Methods.FirstOrDefault(m => m.Name == "Start");
                    if (startMethod == null)
                    {
                        // If Awake/OnEnable/Start method does not exists, overwrite ctors
                        var ctors = type.GetConstructors();
                        foreach (var ctor in ctors)
                        {
                            WriteIlCode(ctor.Body, ctorMethod);
                        }
                    }
                    else
                    {
                        // Otherwise just overwrite Awake
                        WriteIlCode(startMethod.Body, awakeMethod);
                    }
                }
                else
                {
                    // Otherwise just overwrite Awake
                    WriteIlCode(startMethod.Body, awakeMethod);
                }
            }
            else
            {
                // Otherwise just overwrite Awake
                WriteIlCode(startMethod.Body, awakeMethod);
            }
        }

        /// <summary>
        /// Write IL Code in method
        /// </summary>
        public static void WriteIlCode(MethodBody body, MethodDefinition def)
        {
            var ilProcessor = body.GetILProcessor();
                                   
            ilProcessor.InsertBefore(body.Instructions[0],
                ilProcessor.Create(OpCodes.Call, def));
                                     
            ilProcessor.InsertBefore(body.Instructions[0],
                ilProcessor.Create(OpCodes.Ldarg_0));
        }
    }
}