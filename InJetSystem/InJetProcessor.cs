﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ITnnovative.InJet.Attributes;
using ITnnovative.InJetSystem.Attributes;
using UnityEditor;
using UnityEngine;
using Object = System.Object;

namespace ITnnovative.InJetSystem
{
   
    public class InJetProcessor : MonoBehaviour
    { 
        /// <summary>
        /// True if debug enabled
        /// </summary>
        public static bool debug = true;
        
        /// <summary>
        /// List of all InJet singletons
        /// </summary>
        private static List<object> _singletons = new List<object>();
        
        /// <summary>
        /// List of all InJet mono instanced objects
        /// </summary>
        private static List<Type> _monoInstanced = new List<Type>();

        /// <summary>
        /// List of all InJet instanced objects
        /// </summary>
        private static List<Type> _instanced = new List<Type>();
        
        /// <summary>
        /// Processing actions to be done
        /// </summary>
        private static readonly List<Action> _processActions = new List<Action>();

        /// <summary>
        /// If true then will not load singletons
        /// </summary>
        private static bool _singletonsLoaded = false;
        
        /// <summary>
        /// Loads all singletons
        /// </summary>
        public static void LoadSingletons()
        {
            // Check if singletons are not already loaded
            if (_singletonsLoaded) return;
            
            // Process all assemblies
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            foreach (var assembly in assemblies) 
            {
                // And all types in assemblies
                foreach (var type in assembly.GetTypes())
                {
                    // If type is related to singleton
                    var singleton = type.GetCustomAttributes(typeof(InJetSingletonAttribute), false);
                    if (singleton.Length > 0)
                    {
                        if(debug) Debug.Log("[InJet] Loading singleton: " + type);
                        
                        // If object is MonoBehaviour
                        if (type.IsSubclassOf(typeof(MonoBehaviour)))
                        {
                            // Search for object
                            var obj = UnityEngine.Object.FindObjectOfType(type);
                            if (!obj)
                            {
                                obj = new GameObject("InJet_" + type).AddComponent(type);
                            }
                            _singletons.Add(obj);    
                        }
                        else
                        {
                            var obj = Activator.CreateInstance(type);
                            _singletons.Add(obj);
                        }
                        
                    }

                    var instanced = type.GetCustomAttributes(typeof(InJetInstanceAttribute), false);
                    if (instanced.Length > 0)
                    {
                        if(debug) Debug.Log("[InJet] Loading instanced: " + type);
                        // If object is MonoBehaviour
                        if (type.IsSubclassOf(typeof(MonoBehaviour)))
                        {
                            _monoInstanced.Add(type);
                        }
                        else
                        {
                            _instanced.Add(type);
                        }
                    }
                }
            }

            _singletonsLoaded = true;
        }

        public static void ProcessUnity(object obj)
        {
            // Try to load singletons
            LoadSingletons();

            // Process fields
            foreach (var field in obj.GetType().GetFields((BindingFlags)int.MaxValue))
            {
                if (field.GetCustomAttributes(typeof(InJetAttribute)).Any())
                {
                    var singleton = _singletons.FirstOrDefault(s => s.GetType() == field.FieldType);
                    
                    // Singleton was found
                    if (singleton != null)
                    {
                        field.SetValue(obj, singleton);
                    }
                    else
                    {
                        // No singleton was found, check if it's an instanced object?
                        var mi = _monoInstanced.FirstOrDefault(q => q == field.FieldType);
                        var ni = _instanced.FirstOrDefault(q => q == field.FieldType);
                        if (mi != null)
                        {
                            var iObj = new GameObject("TempInstance");
                            var mComp = iObj.AddComponent(mi);
                            
                            field.SetValue(obj, mComp);
                        }
                        else if (ni != null)
                        {
                            var inst = Activator.CreateInstance(ni);
                            field.SetValue(obj, inst);
                        }
                        else
                        {
                            Debug.LogError("[InJet] This type is not supported. Unsupported type: " + field.FieldType);
                        }
                    }
                }
            }
        }
        
        /// <summary>
        /// Process specified behaviour
        /// </summary>
        public static void Process(object obj)
        {
            _processActions.Add(() => ProcessUnity(obj));
        }

        /// <summary>
        /// Processing thread
        /// </summary>
        public void FixedUpdate()
        {
            foreach (var action in _processActions)
            {
                action.Invoke();
            }
            _processActions.Clear();
        }
    }
}