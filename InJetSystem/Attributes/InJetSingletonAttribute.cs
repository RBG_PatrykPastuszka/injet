﻿using System;

namespace ITnnovative.InJetSystem.Attributes
{
    /// <summary>
    /// Represents InJet Singleton - game will use single instance of InJet Singleton in InJets
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class InJetSingletonAttribute : Attribute
    {
    
    }
}
