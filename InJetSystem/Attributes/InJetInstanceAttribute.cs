﻿using System;
using UnityEngine;

namespace ITnnovative.InJet.Attributes
{
    /// <summary>
    /// Represents Instance of InJet - InJet will create new instance of specified type for every
    /// new InJet
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class InJetInstanceAttribute : Attribute
    {
       
    }
}
