﻿using UnityEngine;

namespace ITnnovative.InJetSystem
{
    public static class MonoExtensions
    {
        public static void ProcessInJetCtor(this object obj)
        {
            // Process object :D
            InJetProcessor.Process(obj);
        }
         
        public static void ProcessInJetAwake(this object obj)
        {
            // Process object :D
            InJetProcessor.ProcessUnity(obj);
        }
    }
}