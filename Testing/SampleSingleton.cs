﻿using ITnnovative.InJetSystem.Attributes;
using UnityEngine;

namespace ITnnovative.Testing
{
    [InJetSingleton]
    public class SampleSingleton : MonoBehaviour
    {
        [InJet] public SampleCreatedInstance testProp;

        public void FixedUpdate()
        { 
            Debug.Log("Started");
            Debug.Log(testProp);
        }
    }
}