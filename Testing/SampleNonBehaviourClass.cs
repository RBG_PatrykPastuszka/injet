﻿using System;
using ITnnovative.InJetSystem.Attributes;
using UnityEngine;

namespace ITnnovative.Testing
{
    [Serializable]
    public class SampleNonBehaviourClass
    {
        [InJet]
        public SampleSingleton sample;
    }
}
